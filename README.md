# A-Robust-Counting-and-Measurement-Approach-for-Stacked-Substrates
A Robust Counting and Measurement Approach for Stacked Substrates
https://hdl.handle.net/11296/897dm9

# Demo Video
[YZU x ChinPoon PCB counter](https://drive.google.com/file/d/1uhdzZAJUY4XimqXlQCxBxrTB6Oor3e4X/view?usp=sharing)  
[Carton localisation with PCB](https://drive.google.com/file/d/1guI9yLatGR29j8i_4tXax2U9tpNbJwL-/view?usp=sharing)

